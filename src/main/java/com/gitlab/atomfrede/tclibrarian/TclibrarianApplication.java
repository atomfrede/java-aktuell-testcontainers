package com.gitlab.atomfrede.tclibrarian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TclibrarianApplication {

	public static void main(String[] args) {
		SpringApplication.run(TclibrarianApplication.class, args);
	}

}
